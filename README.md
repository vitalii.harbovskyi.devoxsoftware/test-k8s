######Install Azure CLI
- az login
######Create a resource group
- az group create --name myResourceGroup --location eastus
######Create AKS cluster
- az aks create --resource-group myResourceGroup --name myAKSCluster --node-count 1 --enable-addons monitoring --generate-ssh-keys
######Connect to the cluster
- az aks install-cli
- az aks get-credentials --resource-group myResourceGroup --name myAKSCluster
- kubectl get nodes
- kubectl apply -f gitlab-service-account.yaml


######GitLab variables
######CERTIFICATE_AUTHORITY_DATA - from k8s config
######SERVER - from k8s config
######USER_TOKEN from gitlab-service-account secret kubectl get secrets kubectl describe secret [NAME]

######Create namespace
- kubectl create namespace <your-namespace>

######Create static ip
- az network public-ip create --resource-group MC_myResourceGroup_myAKSCluster_eastus --name myAKSPublicIP --sku Standard --allocation-method static --query publicIp.ipAddress -o tsv
######Install nginx-ingress controller
- helm install nginx-ingress stable/nginx-ingress --namespace <your-namespace> --set controller.service.loadBalancerIP="20.42.38.31"
######Create loadbalancer with nginx
- kubectl apply -f loadbalancer.yml
######Create secret for gitlab repository connection
- kubectl create secret docker-registry influencerinvite-regcred --docker-server=registry.gitlab.com --docker-username=<gitlab-token-name> --docker-password=<gitlab-token-pass> --namespace <your-namespace> 

